﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AutoFixture.Behaviour.Extensions
{
    public static class ReflectionExtensions
    {
        public static string GetPropertyPath(this PropertyInfo pi)
        {
            return $"{pi.DeclaringType.Name}/{pi.Name}";
        }

        public static PropertyInfo GetPropertyInfo<TEntity, TMember>(this Expression<Func<TEntity, TMember>> expression)
        {
            if (expression.Body is MemberExpression)
            {
                return ((MemberExpression)expression.Body).Member as PropertyInfo;
            }
            var op = ((UnaryExpression)expression.Body).Operand;
            return ((MemberExpression)op).Member as PropertyInfo;
        }
    }
}
