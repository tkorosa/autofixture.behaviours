﻿using System.Collections.Generic;

namespace AutoFixture
{
    internal class Permutation
    {
        public List<object> Values;
        private int _position;
        private Permutation _next;

        internal Permutation(List<object> values)
        {
            Values = values;
        }

        internal object GetCurrentValue()
        {
            return Values[_position];
        }


        internal void SetNext(Permutation next)
        {
            _next = next;
        }

        internal void IncreasePosition()
        {
            _position++;
            if (_position >= Values.Count)
            {
                _position = 0;
                _next?.IncreasePosition();
            }
        }
    }
}
