﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using AutoFixture.Behaviour.Extensions;
using AutoFixture.Kernel;

namespace AutoFixture
{
    public partial class PermutationBehavior<TEntity> : ISpecimenBuilder
    {
        private Dictionary<string, Permutation> _permutationCache = new Dictionary<string, Permutation>();
        private List<Permutation> _permutationList = new List<Permutation>();

        public object Create(object request,
                ISpecimenContext context)
        {
            Permutation p;
            var pi = request as PropertyInfo;
            if (pi==null || !_permutationCache.TryGetValue(pi.GetPropertyPath(), out p))
            {
                return new NoSpecimen();
            }
            if (!_permutationList.Contains(p))
            {
                _permutationList.LastOrDefault()?.SetNext(p);
                _permutationList.Add(p);
            }
            else if (p == _permutationList[0])
            {
                p.IncreasePosition();
            }
            return p.GetCurrentValue();
        }

        public PermutationBehavior<TEntity> AddPermutation<TMember>(Expression<Func<TEntity, TMember>> expression, List<TMember> values)
        {
            var pi = expression.GetPropertyInfo();
            _permutationCache.Add(
                pi.GetPropertyPath(), 
                new Permutation(values.Cast<object>().ToList())
            );
            return this;
        }

        public PermutationBehavior<TEntity> AddPermutation<TMember>(Expression<Func<TEntity, TMember>> expression)
            where TMember : struct, IConvertible
        {
            var pi = expression.GetPropertyInfo();
            _permutationCache.Add(
                pi.GetPropertyPath(),
                new Permutation(Enum.GetValues(pi.PropertyType).Cast<object>().ToList())
            );
            return this;
        }

        public int Count()
        {
            return _permutationCache
                .Values
                .Select(v => v.Values.Count)
                .Aggregate((result, amount) => result * amount);
        }
    }
}
