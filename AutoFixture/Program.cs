﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AutoFixture
{
    class Program
    {
        static void Main(string[] args)
        {
            var fixture = new Fixture();
            var permutation =
                new PermutationBehavior<Customer>()
                    .AddPermutation(c => c.MaritalStatus)
                    .AddPermutation(c => c.Address.Country.Continent)
                    .AddPermutation(c => c.Status)
                    ;
            fixture.Customizations.Add(permutation);
            fixture.Customize<Customer>(c =>
                c.With(
                    x => x.OrderIdList,
                    () => NewMethod(fixture)
                )
            );

            var result = fixture
                .CreateMany<Customer>(permutation.Count())
                .Take(permutation.Count())
                .ToList();
        }
        static Random rndSeed = new Random();

        private static List<Guid> NewMethod(Fixture fixture)
        {
            var rnd = rndSeed.Next(3, 10);
            Console.WriteLine(rnd);
            return fixture.CreateMany<Guid>(rnd).ToList();
        }
    }
}
