﻿using System;
using System.Collections.Generic;

namespace AutoFixture
{
    public class Customer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public Status Status { get; set; }
        public Address Address { get; set; }
        public List<Guid> OrderIdList { get; set; }
    }
}
