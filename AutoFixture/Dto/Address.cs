﻿using System.ComponentModel.DataAnnotations;

namespace AutoFixture
{
    public class Address
    {
        public Country Country { get; set; }
        [Range(1000, 9999)]
        public int Zip { get; set; }
        public string Street { get; set; }
    }
}
