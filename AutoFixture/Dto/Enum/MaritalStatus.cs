﻿namespace AutoFixture
{
    public enum MaritalStatus
    {
        Single,
        Married,
        Widowed
    }
}
