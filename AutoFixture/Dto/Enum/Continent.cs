﻿namespace AutoFixture
{
    public enum Continent
    {
        Europe,
        Asia,
        Australia,
        NorthAmerica,
        SouthAmerica,
        Antarctis,
        Arctis
    }
}
