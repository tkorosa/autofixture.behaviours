﻿namespace AutoFixture
{
    public enum Status
    {
        Active,
        Inactive,
        Renewed
    }
}
