﻿using System.ComponentModel.DataAnnotations;

namespace AutoFixture
{
    public class Country
    {
        [StringLength(2)]
        public string Code { get; set; }
        public string Name { get; set; }
        public Continent Continent { get; set; }
    }
}
