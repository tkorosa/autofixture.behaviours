﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoFixture
{
    class Class1
    {
        public Class1()
        {
            var x = new List<int> { 1, 2, 3 };
            var y = x.FirstOrDefault();
        }

        private bool isInputOne(int b)
        {
            return b == 1;
        }
    }
}
